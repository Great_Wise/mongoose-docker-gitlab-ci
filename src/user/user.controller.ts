import { Delete, Get, Param, Put } from '@nestjs/common';
import { Body, Controller, Post } from '@nestjs/common';
import { UserDto } from 'src/dto/user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService:UserService,
    ){}

    @Post('create')
    async createUser (@Body() createUserDto:UserDto):Promise<any> {
        return await this.userService.create(createUserDto);
    }

    @Get('findAll')
    async findAllUsers ():Promise<any> {
        return await this.userService.findAll();
    }

    @Put('update/:id')
    async test ():Promise<any> {
        return 'update test';
    }
    @Delete('DeleteUser')
    async test2 ():Promise<any> {
        return 'delete test';
    }

    @Get('finfOne/:id')
    async findOneUser (@Param('id') id: string ):Promise<any> {
        return await this.userService.findOne(id);
    }

}
